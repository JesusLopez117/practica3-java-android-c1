package com.example.practica3c1java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Declaracion de variables tipo boton
    private Button btnIngresar;
    private Button btnCerrar;

    //Declaracion de variables tipo EditText
    private EditText txtUsuario;
    private EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    //Funcion para iniciar los componentes
    private void iniciarComponentes(){
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
    }

    //Funcion para ingresar
    private void ingresar(){
        //Declaracion de variable
        String strUser;
        String strPassword;

        //Asignar los string
        strUser = "daniel";
        strPassword = "1234";

        //Validacion del usuario y contraseña
        if(txtUsuario.getText().toString().equals(strUser) &&
                txtPassword.getText().toString().equals(strPassword)){
            //Hacer el paquete de datos que se enviaran
        Bundle bundle = new Bundle();
        bundle.putString("usuario", strUser);

            Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            txtUsuario.setText("");
            txtPassword.setText("");
        }else{
            Toast.makeText(getApplicationContext(),
                    "Usuario o contraseña no válidos", Toast.LENGTH_LONG).show();
        }
    }
}








