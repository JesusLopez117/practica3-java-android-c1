package com.example.practica3c1java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculadoraActivity extends AppCompatActivity {
    //Declaración de las variables tipo boton
    private Button btnSumar;
    private Button btnRestar;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;

    //Declaracion de las variables tipo EditText
    private EditText txtNum1;
    private EditText txtNum2;

    //Declaracion de la variable tipo TextView
    private TextView lblResultado;

    //Declaracion de la clase calculadora
    private Calculadora calculadora = new Calculadora(0.0F, 0.0F);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        iniciarComponentes();

        btnSumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnSumar();
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnRestar();
            }
        });

        btnMultiplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnMultiplicar();
            }
        });

        btnDividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnDividir();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    //Funcion donde se relaciona la vista con el Java
    private void iniciarComponentes(){
        btnSumar = (Button) findViewById(R.id.btnSumar);
        btnRestar = (Button) findViewById(R.id.btnRestar);
        btnMultiplicar = (Button) findViewById(R.id.btnMultiplicar);
        btnDividir = (Button) findViewById(R.id.btnDividir);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);

        lblResultado = (TextView) findViewById(R.id.lblResultado);
    }

    //Funcion de los botones


    private void btnSumar(){
        calculadora.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        calculadora.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.sumar()));
    }

    private void btnRestar(){
        calculadora.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        calculadora.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.restar()));
    }

    private void btnMultiplicar(){
        calculadora.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        calculadora.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.multiplicar()));
    }

    private void btnDividir(){
        calculadora.setNum1(Float.parseFloat(txtNum1.getText().toString()));
        calculadora.setNum2(Float.parseFloat(txtNum2.getText().toString()));
        lblResultado.setText(String.valueOf(calculadora.dividir()));
    }

    //Funcion para limpiar la pantalla
    private void limpiar(){
        txtNum1.setText("");
        txtNum2.setText("");
        txtNum1.requestFocus();
        txtNum2.requestFocus();
        lblResultado.setText("");
    }

    //Funcion para regresar
    private void regresar(){
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora");
        confirmar.setMessage(" ¿Desea regresar? ");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        confirmar.show();
    }
}