package com.example.practica3c1java;

public class Calculadora {
    //Declaración de variables
    private float num1 = 0.0F;
    private float num2 = 0.0F;

    //Constructor
    public Calculadora(float num1, float num2){
        this.num1 = num1;
        this.num2 = num2;
    }

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    //Funciones de la clase
    public float sumar(){
        return num1 + num2;
    }

    public float restar(){
        return num1 - num2;
    }

    public float multiplicar(){
        return num1 * num2;
    }

    public float dividir(){
        float total = 0.0F;

        if (num2 != 0.0F){
            return num1 / num2;
        }

        return total;
    }

}
